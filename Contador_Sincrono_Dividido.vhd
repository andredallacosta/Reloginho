LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.std_logic_arith.all;
USE IEEE.std_logic_unsigned.all;

ENTITY Contador_Sincrono_Dividido IS
port(
	reset: in std_logic;
	clock, comeco: in std_logic;
	Ain, Bin, Cin, Din, Ein, Fin: std_LOGIC_VECTOR(3 downto 0);
	Q0, Q1, Q2, Q3, Q4, Q5: out std_logic_vector(6 downto 0)
	);
end Contador_Sincrono_Dividido;

architecture arch_Contador_Sincrono_Dividido of Contador_Sincrono_Dividido is
signal new_clock: std_logic := '0';
signal A, B, C, D, E, F : STD_LOGIC_VECTOR(3 DOWNTO 0) := "0000";

component display_7
port(
		A: in std_logic_vector(3 downto 0);
		S: out std_logic_vector(6 downto 0)	
	);
end component;

component Divisor_Freq
port(
		 CLK: in std_logic;
		 COUT: out std_logic
	);
end component;

begin

Divisor: Divisor_Freq
	port map(CLK => clock, cout => new_clock);

process (new_clock)
begin
	if reset = '1' then
		A <= "0000";
		B <= "0000";
		C <= "0000";
		D <= "0000";
		E <= "0000";
		F <= "0000";
	elsif (comeco = '0')then
		A <= Ain;
		B <= Bin;
		C <= Cin;
		D <= Din;
		E <= Ein;
		F <= Fin;
	else
		if(new_clock'EVENT and new_clock = '1') then
			if(A < "1001")then
				A <= A + 1;
			else
				A <= "0000";
				if(B < "0101")then
					B <= B + 1;
				else
					B <= "0000";
					A <= "0000";
					if(C < "1001")then
						C <= C + 1;
					else
						C <= "0000";
						B <= "0000";
						C <= "0000";
						if(D < "0101")then
							D <= D + 1;
						else
							D <= "0000";
							C <= "0000";
							B <= "0000";
							A <= "0000";
							if(E < "1001")then
								E <= E + 1;
							else
								E <= "0000";
								D <= "0000";
								C <= "0000";
								B <= "0000";
								A <= "0000";
								if(F < "0010")then
									F <= F + 1;
								end if;
							end if;
							if((F = "0010") and (E = "0011") and (D = "0101") and (C = "1001") and (B = "0101") and (A = "1001"))then
									F <= "0000";
									E <= "0000";
									D <= "0000";
									C <= "0000";
									B <= "0000";
									A <= "0000";
							end if;
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;
end process;

SEGUNDOS: display_7
	port map(A => A, S => Q0);
SEGUNDOS2: display_7
	port map(A => B, S => Q1);
MINUTOS: display_7
	port map(A => C, S => Q2);
MINUTOS2: display_7
	port map(A => D, S => Q3);
HORAS: display_7
	port map(A => E, S => Q4);
HORAS2: display_7
	port map(A => F, S => Q5);
	
end arch_Contador_Sincrono_Dividido;
